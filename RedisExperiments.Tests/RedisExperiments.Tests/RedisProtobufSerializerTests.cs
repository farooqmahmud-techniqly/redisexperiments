﻿using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Protobuf;

namespace RedisExperiments.Tests
{
    public sealed class RedisProtobufSerializerTests : RedisSerializationTestsBase
    {
        public RedisProtobufSerializerTests() : base(new ProtobufSerializer())
        {
        }
    }
}