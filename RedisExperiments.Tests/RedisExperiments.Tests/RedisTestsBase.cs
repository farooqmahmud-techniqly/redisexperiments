﻿using System;
using System.Configuration;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace RedisExperiments.Tests
{
    public abstract class RedisTestsBase : IDisposable
    {
        private readonly IDatabase _db;

        protected RedisTestsBase(ISerializer serializer)
        {
            var host = ConfigurationManager.AppSettings["redis:host"];
            var post = int.Parse(ConfigurationManager.AppSettings["redis:port"]);

            var mux = ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = {{host, post}},
                    AllowAdmin = true
                });

            Client = new StackExchangeRedisCacheClient(mux, serializer);
            _db = Client.Database;
        }

        protected ICacheClient Client { get; }

        public void Dispose()
        {
            _db.FlushDatabase();
            _db.Multiplexer.Dispose();
            Client.Dispose();
        }
    }
}