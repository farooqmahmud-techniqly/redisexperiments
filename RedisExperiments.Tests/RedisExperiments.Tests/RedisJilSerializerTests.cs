﻿using Jil;
using StackExchange.Redis.Extensions.Jil;

namespace RedisExperiments.Tests
{
    public sealed class RedisJilSerializerTests : RedisSerializationTestsBase
    {
        public RedisJilSerializerTests() : base(
            new JilSerializer(Options.CamelCase))
        {
        }
    }
}