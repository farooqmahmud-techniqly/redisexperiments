﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Redis.Extensions.Newtonsoft;

namespace RedisExperiments.Tests
{
    public sealed class RedisNewtonsoftSerializerTests : RedisSerializationTestsBase
    {
        public RedisNewtonsoftSerializerTests() : base(
            new NewtonsoftSerializer(new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()}))
        {
        }
    }
}