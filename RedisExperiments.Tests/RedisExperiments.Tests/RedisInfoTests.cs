﻿using System.Configuration;
using System.Threading.Tasks;
using Moq;
using StackExchange.Redis.Extensions.Core;
using Xunit;

namespace RedisExperiments.Tests
{
    public sealed class RedisInfoTests : RedisTestsBase
    {
        public RedisInfoTests() : base(new Mock<ISerializer>().Object)
        {
        }

        [Fact]
        public async Task InfoCommandTests()
        {
            var info = await Client.GetInfoAsync();
            var expectedPort = int.Parse(ConfigurationManager.AppSettings["redis:port"]);
            Assert.Equal(int.Parse(info["tcp_port"]), expectedPort);
        }
    }
}