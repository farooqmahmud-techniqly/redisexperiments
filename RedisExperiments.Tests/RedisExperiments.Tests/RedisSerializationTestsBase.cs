﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FluentAssertions;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Newtonsoft;
using Xunit;

namespace RedisExperiments.Tests
{
    public abstract class RedisSerializationTestsBase : RedisTestsBase
    {
        protected RedisSerializationTestsBase(ISerializer serializer) : base(serializer)
        {
        }

        [Fact]
        public async Task SerializerTest()
        {
            var key = GetKey();
            var expectedItem = new Dictionary<string, int> {{"foo", 1}};
            await Client.AddAsync(key, expectedItem);

            var actualItem = await Client.GetAsync<Dictionary<string, int>>(key);
            Assert.Equal(expectedItem["foo"], actualItem["foo"]);
        }

        [Fact]
        public async Task HashSetAsync_SerializeNullableType()
        {
            var key = GetKey();
            bool? val = null;
            
            if (Client.Serializer.GetType() == typeof(NewtonsoftSerializer))
            {
                await Assert.ThrowsAsync<NullReferenceException>(async () =>
                 {
                     await Client.HashSetAsync(key, new Dictionary<string, bool?> { { "val", val } });
                 });
            }
            else
            {
                await Client.HashSetAsync(key, new Dictionary<string, bool?> { { "val", val } });
                var actualItem = await Client.HashGetAsync<bool?>(key, "val");
                Assert.Null(actualItem);
            }
        }

        [Fact]
        public async Task AddAsync_SerializeNullableType()
        {
            var key = GetKey();
            bool? val = null;

            if (Client.Serializer.GetType() == typeof(NewtonsoftSerializer))
            {
                await Assert.ThrowsAsync<NullReferenceException>(async () =>
                {
                    await Client.AddAsync(key, val);
                });
            }
            else
            {
                await Client.AddAsync(key, val);
                var actualItem = await Client.GetAsync<bool?>(key);
                Assert.Null(actualItem);
            }
        }

        private string GetKey()
        {
            return $"item:{Client.Serializer.GetType().Name}:{Guid.NewGuid()}";
        }
    }
}